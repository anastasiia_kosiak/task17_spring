package com.epam.task1;

import com.epam.task1.config.ConfigurationA;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ConfigurationA.class);
        for (String beanDefinitionName : context.getBeanDefinitionNames()) {
            var beanDefinition = context.getBeanDefinition(beanDefinitionName);
            System.out.println(beanDefinition);
        }
        context.close();
    }
}