package com.epam.task1.bean;

import com.epam.task1.validation.BeanValidator;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA  implements InitializingBean, DisposableBean, BeanValidator{
    private String name;
    private int value;
    public BeanA(BeanB beanB, BeanC beanC) {
        this.name = beanB.getName();
        this.value = beanC.getValue();
    }

    public BeanA(BeanB beanB, BeanD beanD) {
        this.name = beanB.getName();
        this.value = beanD.getValue();
    }

    public BeanA(BeanC beanC, BeanD beanD) {
        this.name = beanC.getName();
        this.value = beanD.getValue();
    }

    public BeanA() { }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("Bean A: afterPropertiesSet()");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("Bean A: destroy()");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean validate() {
        return name.isEmpty() || value >= 0;
    }
}
