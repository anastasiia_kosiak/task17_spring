package com.epam.task1.bean;

import com.epam.task1.validation.BeanValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BeanD implements BeanValidator {
    @Value("${beanD.name}")
    private String name;

    @Value("${beanD.value}")
    private int value;

    public void init() {
        System.out.println("Bean D init method");
    }

    private void destroy() {
        System.out.println("Bean D destroy method");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
    @Override
    public boolean validate() {
        return name.isEmpty() || value >= 0;
    }

}