package com.epam.task1.bean;

import com.epam.task1.validation.BeanValidator;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class BeanE implements BeanValidator {
    private String name;

    private int value;

    public BeanE(BeanA beanA) {
        this.name = beanA.getName();
        this.value = beanA.getValue();
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("Bean E: postConstruct()");
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("Bean E: preDestroy()");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean validate() {
        return name.isEmpty() || value >= 0;
    }
}