package com.epam.task1.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.stereotype.Component;

@Component
public class CustomBeanFactoryPostProcessor implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        BeanDefinitionRegistry registry = (BeanDefinitionRegistry) configurableListableBeanFactory;
        String beanName = BeanB.class.getSimpleName().substring(0, 1).toLowerCase()
                + BeanB.class.getSimpleName().substring(1);
        GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
        beanDefinition.setBeanClass(BeanB.class);
        beanDefinition.setInitMethodName("customInit");
        beanDefinition.setDestroyMethodName("destroy");
        beanDefinition.setDependsOn("beanD");
        registry.registerBeanDefinition(beanName, beanDefinition);
    }
}