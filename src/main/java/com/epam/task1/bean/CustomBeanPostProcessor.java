package com.epam.task1.bean;

import com.epam.task1.validation.BeanValidator;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof BeanValidator) {
            if (((BeanValidator) bean).validate()) {
                System.out.println(beanName + " is valid!");
            }
        }
        return bean;
    }
}
