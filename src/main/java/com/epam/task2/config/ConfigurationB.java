package com.epam.task2.config;

import com.epam.task2.beans2.NarcissusFlower;
import com.epam.task2.beans2.RoseFlower;
import com.epam.task2.beans3.BeanD;
import com.epam.task2.beans3.BeanF;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScans({
        @ComponentScan(basePackages = "com.epam.task2.beans2",
                includeFilters = {
                        @ComponentScan.Filter(type = FilterType.REGEX, pattern = "Flower$"),
                        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {
                                BeanD.class, BeanF.class
                        })
                })
})
public class ConfigurationB {
    @Bean
    public BeanD beanD() {
        return new BeanD();
    }

    @Bean
    public BeanF beanF() {
        return new BeanF();
    }

    @Bean
    public NarcissusFlower narcissusFlower() {
        return new NarcissusFlower();
    }

    @Bean
    public RoseFlower roseFlower() {
        return new RoseFlower();
    }
}