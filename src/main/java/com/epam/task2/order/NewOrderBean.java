package com.epam.task2.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class NewOrderBean {
    @Autowired
    @Qualifier("poppy")
    private Flower poppy;

    @Autowired
    @Qualifier("bluebell")
    private Flower bluebell;

    @Autowired
    private Flower rose;
}