package com.epam.task2.order.implementation;

import com.epam.task2.order.Flower;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
//@Order(1)
@Primary
public class Rose implements Flower {
    private final String name = "Rose";

    @Override
    public String getName() {
        return name;
    }
}
