package com.epam.task2.other;

import org.springframework.stereotype.Component;

@Component
public class OtherBeanB {
    private String name = "Other bean B";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
